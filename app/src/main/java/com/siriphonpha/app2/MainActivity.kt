package com.siriphonpha.app2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupActionBarWithNavController

class MainActivity : AppCompatActivity() {
    //เพิ่มในส่วนของ main
    private lateinit var navController: NavController
    //ถึงตรงนี้
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //เพิ่มส่วนนี้ navHoast เป็นปุ่มกลับหน้าก่อนหน้า
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navHostFragment.navController
        setupActionBarWithNavController(navController)
        //ถึงตรงนี้
    }
    //เพิ่มส่วนนี้ override onsupport
    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp() || super.onSupportNavigateUp()
    }
    //ถึงตรงนี้
}